# Availability, availability, availability!

This SARNET agent demonstration focuses on availability-related attacks.


## Autonomy and coordination

* Can SARNET agents perform conflicting operations?


## Scenario(s)


* Require multi-domain coordination

* Realism: a transit network should be connected to multiple services, an
  attack to one domain should not affect other domains (or rather, the applied
  solution shouldn't make things worse for those domains)

Topology:


               A            B
    [attacker] -> [transit] -> [service]
                    | C
                    |
                  [xdomain]

* A=1gbit B=100mbit:
    * Attacker congests links in transit domain, which will affect B and C
      (reduction in traffic); transit domain will detect attack itself
    * Attacker congests B, not likely to affect C; service domain should
      contact transit domain for efficient resolution

* A <= B: similar to first situation above.



# Control loop


## Detect

The detect "phase" occurs when events (e.g. from an IDS) and network,
application, or node metrics are received.  In general, this data comes from
outside the SARNET agent, via a network controller or from another SARNET
agent.

Each datum is related to one (or more?) elements described in the known
topology. In a multi-domain scenario, domains can (partially) share their
topology view with other domains or export data as if coming from a
"network cloud".

Each of these datapoints relates to one or more observables, and when some
predefined/learned threshold is violated the agent must attempt to restore the
observable to its good state.

TODO:
* Should agents within a domain be on a common bus? That way the network
  controller can e.g. share data to multiple agents using pubsub.
  * This would also make it possible to tell external agents (other domains) to
    connect to that same bus and to subscribe to the relevant events


## Classification

Classification is based on the current state, and determine if the network is
under attack and which type(s) of attack are occuring.

Regarding network level attacks: If link rate maxima are known (bps/pps),
congestion is easy to detect. For non-congestion scenarios baseline and margin
values are needed. (Be wary of time-of-day effects, popularity peaks, &c, on
such measurements.)

Classification is currently implemented on the basis of some predefined rules.
These rules are described below.

NB:
* reduced = from baseline, due to congestion
* higher = above baseline


### Specific traffic DDoS

DDoS attacks based on specific types of traffic (which are detectable as such)

Values can be either in terms of bps or pps.

* Likely: event net:link:congested
* Likely: net:flowcount:tcp:dstport=$service reduced
* Likely: net:flowcount higher

Reflected DDoS [or]:
* net:flowcount:udp:srcport=53 higher large-packets
* net:flowcount:udp:srcport=123 higher large-packets
* net:flowcount:icmp:direction=reply

TCP SYN flood:
* net.flowcount:tcp::syn higher
* Especially: ratio net:flowcount:tcp net:flowcount:tcp::syn
* Especially: net:flowcount:tcp:$service:syn higher


### Interesting application-layer DoS

* Request flooding
    * Flowcount anomaly for particular IP
    * Be wary of NAT
* Slowloris
    * Very long & low bandwith flow duration
    * What about normal websockets?


### General anomalies

* Detecting traffic to (non-existing) "services" outside normal baseline may
  be an (early) indication of an attack
* Surviving the "Slashdot effect" without marking legitimate users as attacker


## Analyze

During the analyze phase:

* Map attack to network elements
  * .. which are affected
  * .. which are potentially usable to resolve the attack
* Map attacker origin to entry/transit domains

Implies the SARNET agent either has enough information to make these decisions,
or has some method to request the information from the event source.

May propose multiple strategies.


## Decide, risk

This phase decides on when to start deploying which response and, e.g. when the
attack subsides, when to remove the response.

* Stabilization (a single 1-second spike shouldn't trigger a response)
* Choose when to implement; delay
* Could hook into some risk-tradeoff analysis engine


## React

The response activation and removal steps are fairly straight forward.


### Refine (measure/adjust)

As the attack increases/subsides/changes countermeasures should be scaled
accordingly.



# Control plane limitations

In an ideal world, full flow information is sent to the SARNET agent so that it
can make local decisions.  In reality, both bandwidth constraints and latency
mean this cannot be done (?). The problem becomes worse when flow information
is requested in domains far from the agent.

We are not necessarily interested in every last byte available in the flow
information (or rather, we mostly know what we want ahead of time) and can in
most cases create an aggregate of this data. [0]

In some cases the defensive response should not or (or can not) by fully
"parameterized" by the SARNET agent, and instead the deployed solution should
itself e.g. learn which IPs it should block and so on. [1]

[0] The SARNET agent could construct custom "sensors".

[1] Consider a large scale attack with many participating IPs; classifying each
individual IP at the agent and subsequently synchronizing a list of attacking
IPs with the filtering defense may not be a good solution. It does mean the
filter has to be "smart/active".


# Learning limitations

Computing baselines is nice, but changes made to the application or the network
could require relearning a lot of the metrics used by the agent. (E.g. think of
some website deploying a fancy new UDP-based video service, the increase in UDP
flows/bandwidth could be seen as attack.)


-------------------------------------------------------------------------------

# Protocol


    VNET -> Agent, Agent -> Agent (?)

        $item m $key $value
        $item e $event $value


# Classify vs analyze


